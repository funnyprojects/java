package com.company.secondtask.main;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int algorithmId;
        do {
            algorithmId = enterFromConsole("Choose action: 1-calculating a series of Fibonacci numbers, 2-calculating a factorial");
        } while (algorithmId != 1 && algorithmId != 2);

        int loopType;
        do {
            loopType = enterFromConsole("Choose loop type: 1-while, 2-do-while, 3-for");
        } while (loopType != 1 && loopType != 2 && loopType != 3);

        int num;
        do {
            num = enterFromConsole("Input positive num");
        } while (num <= 0);

        if (algorithmId == 1) {
            int[] fibonachiNums = calculateFibonacciNums(num, loopType);

            System.out.println(String.format("%d Fibonacci numbers", num));
            for (int i = 0; i < fibonachiNums.length; i++) {
                System.out.print(fibonachiNums[i] + " ");
            }
            System.out.println();

        } else if (algorithmId == 2) {
            int factorial = getFactorial(num, loopType);
            System.out.println(String.format("Fatorial of %d = %d", num, factorial));
        }


    }

    public static int enterFromConsole(String message) {

        Scanner sc = new Scanner(System.in);

        System.out.println(message);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.println(message);
        }
        int num = sc.nextInt();

        return num;

    }

    public static int[] calculateFibonacciNums(int num, int loopType) {
        int[] fibonacciNums = new int[num];

        if (num == 1) {
            fibonacciNums[0] = 0;
        } else {
            fibonacciNums[0] = 0;
            fibonacciNums[1] = 1;
            int currentIndex = 2;

            if (fibonacciNums.length > currentIndex) {
                switch (loopType) {
                    case 1:
                        while (currentIndex < num) {
                            fibonacciNums[currentIndex] = fibonacciNums[currentIndex - 1] + fibonacciNums[currentIndex - 2];
                            currentIndex++;
                        }
                        break;
                    case 2:
                        do {
                            fibonacciNums[currentIndex] = fibonacciNums[currentIndex - 1] + fibonacciNums[currentIndex - 2];
                            currentIndex++;
                        } while (currentIndex < num);
                        break;
                    case 3:
                        for (int i = currentIndex; i < fibonacciNums.length; i++) {
                            fibonacciNums[i] = fibonacciNums[i - 1] + fibonacciNums[i - 2];
                        }
                        break;
                }
            }


        }

        return fibonacciNums;
    }

    public static int getFactorial(int num, int loopType) {
        int factorial = 1;
        int currentIndex = 1;

        switch (loopType) {
            case 1:
                while (currentIndex <= num) {
                    factorial *= currentIndex;
                    currentIndex++;
                }
                break;
            case 2:
                do {
                    factorial *= currentIndex;
                    currentIndex++;
                } while (currentIndex <= num);
            case 3:
                for (int i = currentIndex; i <= num; i++) {
                    factorial *= currentIndex;
                }
        }

        return factorial;
    }


}
